/**
 * Created by Troy on 10/26/16.
 */

let Board = require('firmata').Board;

let board = new Board(process.argv[2]);

board.once('ready', () =>
{
	console.log('Connected', board.firmware.name, `${board.firmware.version.major}.${board.firmware.version.minor}`);

	let pins = [2, 3, 4];
	let mode = board.MODES.PULLUP;

	for (let pin of pins)
	{
		console.log('setting pin', pin, 'to', mode);
		board.pinMode(pin, mode);
		console.log('monitoring pin', pin);
		board.digitalRead(pin, (value) => console.log('pin', pin, 'is', value ? 'HIGH' : 'LOW'));
	}
});

setInterval(() => console.log('========== 5s BREAK =========='), 5000);